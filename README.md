# GCW Zero Rust

You want to create a Rust project for the GCW Zero handheld console ?
You're in the good place!

### 1 - Install GCW Zero toolchain
First, you have to install the GCW Zero toolchain.
Go on the website http://www.gcw-zero.com/develop and download the lastest version of the toolchain.

You have to untar/unzip the file under the /opt folder, otherwise the toolchain won't work!

***Warning*: If you're under a 64bits system (well, now mostly are), you have to install those packages (debian/ubuntu):**
```
sudo apt-get install gcc-multilib
sudo apt-get install g++-multilib
```

Once it is done, we will add the toolchain binaries in the PATH:
- Create or open the file ~/.bashrc
- Add the following line :`export PATH=/opt/gcw0-toolchain/usr/bin:$PATH`
- Logout and login, or put `source ~/.bashrc` in your terminal

To test if the toolchain is correctly installed, we can create a simple "hello world" en C++:
```cpp
#include <iostream>

using namespace std;

int main()
{
    cout << "Hello, World!" << endl;
    return 0;
}
```
And then, run the following command: `mipsel-linux-g++ -o hello_world_gcw your_cpp_file.cpp`

Copy the **hello_world_gcw** executable in your GCW, run it, it should show "Hello, World!".

### 2 - Install Rust (or Update)
Install Rust compiler and Cargo by following this link: https://www.rust-lang.org/tools/install, 
or by copying this command in your terminal: `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`

Follow the instructions, then write in your terminal `cargo`. If it is showing cargo help, Rust is install!

**If you have already Rust installed**, but you're not sure to have the last version, write in the terminal:
- `rustup self update`
- `rustup update`

### 3 - Add the *mipsel* target for Rust
We have to install the mipsel target for Rust to compile for our GCW Zero. And it's an easy task thanks to **rustup**!

In the terminal: `rustup target add mipsel-unknown-linux-musl`

Yep, that's all for this step!

### 4 - Create your project and configure it
To create a Rust project, we will use cargo:
`cargo new --vcs none my_project`

That was the easy part, now we need to add some configuration to tell cargo to use the GCW Zero toolchain when we compile for the mipsel *target*:

- In your project folder, add a new folder named **.cargo**
- In the folder **.cargo**, add a file named **config** (without any extention)
- Add the following line in the **config** file:
  ```toml
  # If the target is mipsel, we want to use the GCW zero toolchain
  [target.mipsel-unknown-linux-musl]

  linker = "mipsel-linux-ld"  # Use the GCW Zero toolchain linker
  rustflags = "-C target-feature=+crt-static"  # Link statically C libs 
  ```

We're done with the project configuration!

### 5 - Compile your project
To compile our project, we will use cargo:

You can compile and run the Rust project for your current computer:

- Debug(Build): `cargo build`
- Debug(Run): `cargo run`
- Release(Build): `cargo build --release`
- Release(Run): `cargo run --release`

And you can compile the Rust project for the GCW Zero:

- Debug: `cargo build --target mipsel-unknown-linux-musl`
- Release: `cargo build --release --target mipsel-unknown-linux-musl`

You can now copy your executable in your GCW Zero and execute it! 


### (Bonus) 6 - Make your executable smaller
By default, the output executable could be big (more than 2mb!), even for a simple Hello world.

You can strip this executable by using the following command: `mipsel-linux-strip -s your_executable`

And now, it's just a few kb!
